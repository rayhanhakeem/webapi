﻿using Microsoft.EntityFrameworkCore;
using WebAPIJWT.Models;

namespace WebAPIJWT.Context
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Movie> Movies { get; set; }

	}
}
